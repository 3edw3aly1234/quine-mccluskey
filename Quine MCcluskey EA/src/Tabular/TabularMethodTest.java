/**
 * @author Ali Rihan && Mohamed Eid.
 */
package Tabular;

import java.util.Scanner;

public class TabularMethodTest {

	public int variables() {
		System.out.println("Please enter number of variables:");
		int numOfVariables = 0;
		// Input Check.
		do {
			try {
				@SuppressWarnings("resource")
				Scanner s = new Scanner(System.in);
				numOfVariables = s.nextInt();
				while (numOfVariables <= 0 || numOfVariables > 26) {
					System.out.println("Invalid Input. Please reEnter the number of variables:");
					numOfVariables = s.nextInt();

				}
			} catch (Exception e) {
				System.out.println("Invalid Input. Please reEnter the number of variables:");
			}
		} while (numOfVariables <= 0 || numOfVariables > 26);
		return numOfVariables;
	}

	public int[] minTerms(int numOfVariables) {
		System.out.println("Please Enter minterms in the form: 1 2 4 5 8 12\n" + "a space between them.");
		int maxNumberOfMinterms = (int) Math.pow(2, numOfVariables);
		int[] minTerms = new int[maxNumberOfMinterms];
		int counter = 0;
		do {
			try {
				@SuppressWarnings("resource")
				Scanner s = new Scanner(System.in);
				String input = new String();
				if (s.hasNextLine()) {
					input = s.nextLine();
				}
				@SuppressWarnings("resource")
				Scanner numbers = new Scanner(input);
				counter = 0;
				while (numbers.hasNext()) {
					minTerms[counter++] = numbers.nextInt();
				}
				if (counter != 1) {
					for (int i = 0; i < counter; i++) {
						for (int j = i + 1; j < counter; j++) {
							if (minTerms[i] == minTerms[j]) {
								throw new RuntimeException();
							}
						}
					}
				}
				for (int i = 0; i < counter; i++) {
					if (minTerms[i] >= maxNumberOfMinterms || minTerms[i] < 0) {
						throw new RuntimeException();
					}
				}
			} catch (Exception e) {
				counter = 0;
				System.out.println("Invalid Input. Please reEnter the minTerms:");
			}
		} while (counter == 0 || counter > maxNumberOfMinterms);
		int[] result = new int[counter];
		for (int i = 0; i < counter; i++) {
			result[i] = minTerms[i];
		}
		return result;
	}

	public int[] dontCares(int numOfVariables, int[] minTerms) {
		System.out.println("Please Enter dontcares in the form: 1 2 4 5 8 12\n" + "a space between them.");
		int maxNumberOfMinterms = (int) Math.pow(2, numOfVariables);
		int maxNumberOfDontCares = maxNumberOfMinterms - minTerms.length;
		int[] dontCares = new int[maxNumberOfDontCares];
		int counter = 0;
		do {
			try {
				@SuppressWarnings("resource")
				Scanner s = new Scanner(System.in);
				String input = new String();
				if (s.hasNextLine()) {
					input = s.nextLine();
				}
				@SuppressWarnings("resource")
				Scanner numbers = new Scanner(input);
				counter = 0;
				while (numbers.hasNext()) {
					dontCares[counter++] = numbers.nextInt();
				}
				// checks duplicates.
				if (counter != 1) {
					for (int i = 0; i < counter; i++) {
						for (int j = i + 1; j < counter; j++) {
							if (dontCares[i] == dontCares[j]) {
								throw new RuntimeException();
							}
						}
					}
				}
				// checks duplicates with minterms.
				for (int i = 0; i < counter; i++) {
					for (int j = 0; j < minTerms.length; j++) {
						if (dontCares[i] == minTerms[j]) {
							throw new RuntimeException();
						}
					}
				}

				for (int i = 0; i < dontCares.length; i++) {
					if (dontCares[i] >= maxNumberOfMinterms || dontCares[i] < 0) {
						throw new RuntimeException();
					}
				}
			} catch (Exception e) {
				counter = 0;
				System.out.println("Invalid Input. Please reEnter the don't cares:");
			}
		} while (counter == 0 || counter > maxNumberOfDontCares);
		int[] result = new int[counter];
		for (int i = 0; i < counter; i++) {
			result[i] = dontCares[i];
		}
		return result;
	}

	public static void main(String[] args) {

		TabularMethodTest test = new TabularMethodTest();
		int numOfVariables = test.variables();

		// Entering minTerms.
		int[] minTerms = test.minTerms(numOfVariables);
		int choice = 0;
		do {
			try {
				System.out.println("\nEnter 1 if you want to add don't cares and 2 if you don't:");
				@SuppressWarnings("resource")
				Scanner s = new Scanner(System.in);
				choice = s.nextInt();
			} catch (Exception e) {
				System.out.println("\nEnter 1 if you want to add don't cares and 2 if you don't:");
			}
		} while (choice != 1 && choice != 2);
		int[] dontCares = {};
		if (choice == 1) {
			// Entering don'tcares.
			dontCares = test.dontCares(numOfVariables, minTerms);
		}
		TabularMethod T = new TabularMethod(minTerms, dontCares, numOfVariables);
		
		if(T.mixed.length == (int) Math.pow(2, numOfVariables)){
			System.out.print("\nF = 1");
		}
		
		
		else{
			// Processing.
			
			List binRepresentations = T.process();
			List primeImplicantsWithRepetitions = new List();
			List minTermsInvolved = new List();

			for (int i = 0; i < binRepresentations.size(); i++) {
				List a = ((List) binRepresentations.get(i));
				for (int j = 0; j < a.size(); j++) {
					if (((TabularMethod.matchedPairs) a.get(j)).check == false) {
						String str = ((TabularMethod.matchedPairs) a.get(j)).pattern;
						primeImplicantsWithRepetitions.add(str);
						List b = new List();
						b = ((TabularMethod.matchedPairs) a.get(j)).pairs;
						minTermsInvolved.add(b);
					}
				}
			}

			List.Node N = primeImplicantsWithRepetitions.getHead();
			String[] toArray = new String[primeImplicantsWithRepetitions.size()];

			for (int i = 0; i < toArray.length; i++) {
				toArray[i] = (String) N.getElement();
				N = N.getNext();
			}

			// Processing.

			// ==================================================================

			// Remove Duplicated Prime Implicants.
			List.Node K = primeImplicantsWithRepetitions.getHead();
			List filterTerms = new List();
			boolean flag;
			int finalSize = 0;
			for (int i = 0; i < toArray.length; i++) {
				String temp = (String) K.getElement();
				flag = false;
				for (int j = 0; j < i; j++) {
					if (temp.equals(toArray[j])) {
						filterTerms.add(i);
						flag = true;
						break;
					}
				}
				if (flag == false) {
					toArray[finalSize] = temp;
					finalSize++;
				}
				K = K.getNext();
			}

			String[] primeImplicants = new String[finalSize];

			for (int i = 0; i < finalSize; i++) {
				primeImplicants[i] = toArray[i];
			}

			for (int i = filterTerms.size() - 1; i >= 0; i--) {
				minTermsInvolved.remove((int) filterTerms.get(i));
			}
			// Remove Duplicated Prime Implicants.

			// ==================================================================

			// convert the prime implicants from binary to letters.

			for (int i = 0; i < primeImplicants.length; i++) {
				primeImplicants[i] = T.toLetters(primeImplicants[i]);
			}

			// convert the prime implicants from binary to letters.

			// ==================================================================

			// Creating the final table.

			int[][] table = new int[primeImplicants.length + 1][minTerms.length + 1];
			for (int i = 1; i < primeImplicants.length + 1; i++) {
				table[i][0] = i - 1;
			}
			for (int i = 1; i < minTerms.length + 1; i++) {
				table[0][i] = minTerms[i - 1];
			}

			T.primeImplicants = new String[primeImplicants.length];
			for (int i = 0; i < primeImplicants.length; i++) {
				T.primeImplicants[i] = primeImplicants[i];
			}

			// Creating the final table

			// ==================================================================

			// Displaying Prime Implicants.

			System.out.print("Consider the Function as: F(");
			String Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			for (int i = 0; i < T.numOfVariables; i++) {
				System.out.print(Letters.charAt(i));
				if (i < T.numOfVariables - 1) {
					System.out.print(", ");
				}
			}
			System.out.print(")\n");
			System.out.print("Then,\n");
			System.out.print("\nPrime Implicants:\n\n");
			for (int i = 0; i < primeImplicants.length; i++) {
				System.out.print((i + 1) + ") ");
				System.out.print(primeImplicants[i]);
				System.out.print(" Matched minTerms: ");
				for (int j = 0; j < ((List) (minTermsInvolved.get(i))).size(); j++) {
					int x = (int) ((List) (minTermsInvolved.get(i))).get(j);
					System.out.print("m" + x + " ");
					for (int k = 1; k < minTerms.length + 1; k++) {
						if (x == table[0][k]) {
							table[i + 1][k] = 1;
						}
					}
					if (j < ((List) (minTermsInvolved.get(i))).size() - 1) {
						System.out.print("- ");
					}
				}
				System.out.print("\n");
			}

			// Displaying Prime Implicants.

			// ==================================================================

			T.table = new int[table.length][table[0].length];
			for (int i = 0; i < table.length; i++) {
				for (int j = 0; j < table[0].length; j++) {
					T.table[i] = table[i];
				}
			}

			// test minimum
			T.allPossible();

			System.out.println("\n\nThe steps:");
			for (int i = 0; i < binRepresentations.size(); i++) {
				List a = (List) (binRepresentations).get(i);
				for (int j = 0; j < a.size(); j++) {
					TabularMethod.matchedPairs b = (TabularMethod.matchedPairs) a.get(j);
					System.out.print("\n" + b.pattern + " ");
					List c = b.pairs;
					for (int k = 0; k < c.size(); k++) {
						System.out.print("m" + c.get(k) + " ");
					}
					if (!b.check) {
						System.out.print("Prime Implicant.");
					}
				}
				System.out.println("\n");
			}

			// final table

			System.out.print("\n");
			System.out.print("The covering table:\n\n");
			for (int i = 0; i < primeImplicants.length + 1; i++) {
				for (int j = 0; j < minTerms.length + 1; j++) {
					if (i == 0 && j == 0) {
						System.out.print("         ");
					} else if (i == 0) {
						System.out.print(table[0][j] + " ");
					} else if (j == 0) {
						if (i < 10) {
							System.out.print("prime#" + i + "  ");
						} else {
							System.out.print("prime#" + i + " ");
						}
					} else if (table[i][j] == 1) {
						if (table[0][j] < 10) {
							System.out.print("x ");
						} else {
							System.out.print("x  ");
						}
					} else {
						if (table[0][j] < 10) {
							System.out.print("- ");
						} else {
							System.out.print("-  ");
						}
					}
				}
				System.out.print("\n");
			}

			System.out.println("\n");
			System.out.println("Minimum functions:\n");

			for (int i = 0; i < T.minimum.size(); i++) {
				System.out.print("F" + (i + 1) + " = ");
				for (int j = 0; j < ((List) T.minimum.get(i)).size(); j++) {
					System.out.print(primeImplicants[(int) (((List) T.minimum.get(i)).get(j))]);
					if (j < ((List) T.minimum.get(i)).size() - 1) {
						System.out.print(" + ");
					}
				}
				System.out.print("\n");
			}
		}
	}

}