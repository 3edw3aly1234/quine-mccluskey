/**
 * @author Ali Rihan && Mohamed Eid.
 */
package Tabular;

import ITabular.ILinkedList;

public class List implements ILinkedList {

	public class Node {

		private Object element; // Element stored in this node
		private Node next; // reference To the next node in the list

		public Node(Object o, Node n) {
			element = o;
			next = n;
		}

		/** Returns the element of this node. */
		public Object getElement() {
			return element;
		}

		/** Returns the next node of this node. */
		public Node getNext() {
			return next;
		}

		// Modifier methods:
		/** Sets the element of this node. */
		public void setElement(Object newElem) {
			element = newElem;
		}

		/** Sets the next node of this node. */
		public void setNext(Node newNext) {
			next = newNext;
		}
	}

	private Node head;// head node of the list
	private int size; // number of nodes in the list

	/** Returns the element of this node. */
	public Node getHead() {
		return head;
	}

	/** Default constructor that creates an empty list */
	public List() {
		head = null;
		size = 0;
	}

	@Override
	public void concate(List a) {
		for (int i = 0; i < a.size(); i++) {
			this.add(a.get(i));
		}
	}

	@Override
	public void add(int index, Object element) {
		if (index <= size && index > 0 && element != null) {
			Node newNode = new Node(element, null);
			Node n = head;
			for (int i = 0; i < index - 1; i++) {
				n = n.next;
			}
			Node v = n.next;
			n.next = newNode;
			newNode.next = v;
			size++;
		} else if (index == 0) {
			Node newNode = new Node(element, null);
			newNode.next = head;
			head = newNode;
			size++;
		} else {
			throw null;
		}
	}

	@Override
	public void add(Object element) {
		if (element != null) {
			if (isEmpty()) {
				Node newNode = new Node(element, null);
				head = newNode;
			} else {
				Node newNext = new Node(element, null);
				Node n = head;
				while (n.next != null) {
					n = n.next;
				}
				n.setNext(newNext);
			}
			size++;
		} else {
			throw null;
		}
	}

	@Override
	public Object get(int index) {
		if (index < size && index >= 0) {
			Node n = head;
			for (int i = 0; i < index; i++) {
				n = n.next;
			}
			return n.getElement();
		} else {
			throw null;
		}
	}

	@Override
	public void set(int index, Object element) {
		if (index < size && index >= 0) {
			Node n = head;
			for (int i = 0; i < index; i++) {
				n = n.next;
			}
			n.setElement(element);
		} else {
			throw null;
		}
	}

	@Override
	public void clear() {
		if (size != 0) {
			head = null;
			size = 0;
		}
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void remove(int index) {
		if (index < size && index >= 0) {
			if (index == 0) {
				head = head.next;
			} else {
				Node n = head;
				for (int i = 0; i < index - 1; i++) {
					n = n.next;
				}
				Node v = n;
				n = n.next.next;
				v.setNext(n);
			}
			size--;
		} else {
			throw null;
		}
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public ILinkedList sublist(int fromIndex, int toIndex) {
		if (fromIndex >= 0 && toIndex >= fromIndex && toIndex < size) {
			List sublist = new List();
			for (int i = 0; i < size; i++) {
				if (i >= fromIndex && i <= toIndex) {
					sublist.add(this.get(i));
				}
			}
			return sublist;
		} else {
			throw null;
		}
	}

	@Override
	public boolean contains(Object o) {
		if (o != null) {
			if (size == 0) {
				throw null;
			} else {
				Node n = head;
				while (n != null) {
					if (o.equals(n.getElement())) {
						return true;
					}
					n = n.next;
				}
				return false;
			}
		} else {
			throw null;
		}
	}
}
