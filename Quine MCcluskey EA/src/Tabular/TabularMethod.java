/**
 * @author Ali Rihan && Mohamed Eid.
 */
package Tabular;

import java.lang.Math;

public class TabularMethod {

	int[] minTerms;
	int[] dontCares;
	int numOfVariables;
	int[] mixed;

	String[] primeImplicants;
	int[] index;
	int[][] table;
	int[][] tempTable;
	boolean flag = true;

	List minimum = new List();

	public TabularMethod(int[] minTerms, int[] dontCares, int numOfVariables) {
		this.minTerms = minTerms;
		this.dontCares = dontCares;
		this.numOfVariables = numOfVariables;
		this.mixed = new int[minTerms.length + dontCares.length];
		for (int i = 0; i < minTerms.length; i++) {
			mixed[i] = minTerms[i];
		}
		for (int i = minTerms.length; i < minTerms.length + dontCares.length; i++) {
			mixed[i] = dontCares[i - minTerms.length];
		}
	}

	public class matchedPairs {
		List pairs;
		public String pattern;
		boolean check;

		public matchedPairs(List pairs, String pattern) {
			this.pairs = pairs;
			this.pattern = pattern;
		}

		public matchedPairs() {
		}
	}

	public String toBinary(int num) {
		char[] binary = new char[numOfVariables];
		for (int i = numOfVariables - 1; i >= 0; i--) {
			if (num % 2 == 0) {
				binary[i] = '0';
			} else {
				binary[i] = '1';
			}
			num /= 2;
		}
		String result = new String(binary);
		return result;
	}

	public int numOfOnes(String binary) {
		int counter = 0;
		for (int i = 0; i < binary.length(); i++) {
			if (binary.charAt(i) == '1') {
				counter++;
			}
		}
		return counter;
	}

	public List createTable() {
		List column = new List();
		for (int i = 0; i < mixed.length; i++) {
			List a = new List();
			a.add(mixed[i]);
			matchedPairs m = new matchedPairs();
			m.pairs = a;
			m.pattern = toBinary(mixed[i]);
			column.add(m);
		}
		return column;
	}

	public String compare(String str1, String str2) {
		char[] res = str1.toCharArray();
		int count = 0;
		for (int i = 0; i < str1.length(); i++) {
			if (str1.charAt(i) != str2.charAt(i)) {
				res[i] = '_';
				count++;
			}
			if (count > 1) {
				return null;
			}
		}
		String x = new String(res);
		return x;
	}

	public List process() {
		List columns = new List();
		columns.add(createTable());
		int v = 0;
		boolean pairsFound = true;
		while (pairsFound) {
			List c = new List();
			pairsFound = false;
			int size = ((List) columns.get(v)).size();
			for (int i = 0; i < size; i++) {
				for (int j = i + 1; j < size; j++) {
					String x = ((matchedPairs) ((List) columns.get(v)).get(i)).pattern;
					String y = ((matchedPairs) ((List) columns.get(v)).get(j)).pattern;
					if (Math.abs(numOfOnes(x) - numOfOnes(y)) == 1) {
						String pattern = compare(x, y);
						if (pattern != null) {
							((matchedPairs) ((List) columns.get(v)).get(i)).check = true;
							((matchedPairs) ((List) columns.get(v)).get(j)).check = true;
							List b = new List();
							b.concate(((matchedPairs) ((List) columns.get(v)).get(i)).pairs);
							b.concate(((matchedPairs) ((List) columns.get(v)).get(j)).pairs);
							matchedPairs a = new matchedPairs(b, pattern);
							c.add(a);
							pairsFound = true;
						}
					}
				}
			}
			v++;
			columns.add(c);
		}
		return columns;
	}

	public String toLetters(String binary) {
		String letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String result = "";
		for (int i = 0; i < binary.length(); i++) {
			if (binary.charAt(i) != '_') {
				result += letters.charAt(i);
			}
			if (binary.charAt(i) == '0') {
				result += '`';
			}
		}
		return result;
	}

	public boolean isCovered(int[][] array) {
		for (int i = 1; i < array.length; i++) {
			for (int j = 1; j <= minTerms.length; j++) {
				if (array[i][j] == 1) {
					return false;
				}
			}
		}
		return true;
	}

	public void cover(int[][] array, int row) {
		for (int i = 1; i < minTerms.length + 1; i++) {
			if (array[row + 1][i] == 1) {
				for (int j = 1; j < array.length; j++) {
					array[j][i] = 0;
				}
			}
		}
	}

	public void copyTable() {
		this.tempTable = new int[this.table.length][this.table[0].length];
		for (int i = 0; i < this.tempTable.length; i++) {
			for (int j = 0; j < this.tempTable[0].length; j++) {
				this.tempTable[i][j] = this.table[i][j];
			}
		}
	}

	public void combinations(int[] arr, int len, int startPosition, int[] result) {
		if (len == 0) {
			List temp = new List();
			for (int i = 0; i < result.length; i++) {
				temp.add(result[i]);
			}
			for (int i = 0; i < temp.size(); i++) {
				cover(this.tempTable, (int) (temp.get(i)));
			}
			if (isCovered(this.tempTable) == true) {
				this.flag = false;
				minimum.add(temp);
			}
			copyTable();
			return;
		}
		for (int i = startPosition; i <= arr.length - len; i++) {
			result[result.length - len] = arr[i];
			combinations(arr, len - 1, i + 1, result);
		}
	}

	public void allPossible() {
		this.index = new int[this.primeImplicants.length];
		for (int i = 0; i < index.length; i++) {
			this.index[i] = i;
		}

		for (int i = 1; i < this.table.length; i++) {
			copyTable();
			int[] result = new int[i];
			combinations(index, i, 0, result);
			if (this.flag == false) {
				break;
			}
		}
	}
}
