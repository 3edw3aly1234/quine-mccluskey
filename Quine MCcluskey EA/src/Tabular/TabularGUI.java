/**
 * @author Ali Rihan && Mohamed Eid.
 */
package Tabular;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Scanner;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.Color;


public class TabularGUI {

	private JFrame frame;
	private JTextField MinTermsText;
	private JTextField DontCaresText;
	private JTextField NumOfVariablesText;
	private JTextArea primeImplicantsArea;
	private JTextArea minPossibilitiesArea;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TabularGUI window = new TabularGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TabularGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1325, 748);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		MinTermsText = new JTextField();
		MinTermsText.setBounds(157, 71, 632, 28);
		MinTermsText.setHorizontalAlignment(SwingConstants.CENTER);
		MinTermsText.setColumns(10);

		JLabel MinTermsLabel = new JLabel("Min Terms");
		MinTermsLabel.setBounds(19, 58, 128, 54);
		MinTermsLabel.setFont(new Font("Calibri", Font.PLAIN, 16));
		MinTermsLabel.setHorizontalAlignment(SwingConstants.CENTER);

		JLabel DontCaresLabel = new JLabel("Don't Cares");
		DontCaresLabel.setBounds(19, 103, 128, 54);
		DontCaresLabel.setHorizontalAlignment(SwingConstants.CENTER);
		DontCaresLabel.setFont(new Font("Calibri", Font.PLAIN, 16));

		DontCaresText = new JTextField();
		DontCaresText.setBounds(157, 116, 632, 28);
		DontCaresText.setHorizontalAlignment(SwingConstants.CENTER);
		DontCaresText.setColumns(10);

		JButton SolveButton = new JButton("Solve");
		SolveButton.setBounds(891, 54, 343, 90);
		SolveButton.setFont(new Font("Calibri", Font.PLAIN, 23));
		SolveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// clear the areas
				primeImplicantsArea.setText("");
				minPossibilitiesArea.setText("");
				
				
				// number of variables.
				String varsStr = NumOfVariablesText.getText();
				Scanner v = new Scanner(varsStr);
				v.useDelimiter("[^\\d*\\-*]+");
				int varsNum = 0; // # numberOfVariables
				
				if(v.hasNext()){
					varsNum = Integer.parseInt(v.next());
				}
				v.close();
				
				//=====================================
				
				// minterms.
				String min = MinTermsText.getText();
				Scanner m = new Scanner(min);
				int[] temp1 = new int[100000];
				m.useDelimiter("[^\\d*\\-*]+");
				int counter1 = 0;
				while(m.hasNext()){
					temp1[counter1] = Integer.parseInt(m.next());
					counter1++;
				}
				int[] minTerms = new int[counter1]; // # minTerms
				
				for(int j = 0; j < counter1; j++){
					minTerms[j] = temp1[j];
				}
				m.close();
				
				//=====================================
				
				// don't cares.
				
				String dont = DontCaresText.getText();
				Scanner d = new Scanner(dont);
				int[] temp2 = new int[100000];
				d.useDelimiter("[^\\d*\\-*]+");
				int counter2 = 0;
				while(d.hasNext()){
					temp2[counter2] = Integer.parseInt(d.next());
					counter2++;
				}
				int[] dontCare = new int[counter2]; // # dontCares
				
				for(int j = 0; j < counter2; j++){
					dontCare[j] = temp2[j];
				}
				d.close();
				
				
				//=====================================
				
				if(varsNum <= 0 || varsNum > 26){
					JOptionPane.showMessageDialog(null, "Number of variables should be between 1 and 26 inclusive.");
				}
				
				else if(minTerms.length == 0){
					minPossibilitiesArea.append("F = 0");
				}
				
				else{
					// print the results
					
					TabularMethod T = new TabularMethod(minTerms, dontCare, varsNum);
					
					boolean check = true;
					
					for(int i = 0; i < T.mixed.length - 1; i++){
						for(int j = i + 1; j < T.mixed.length; j++){
							if(T.mixed[i] == T.mixed[j] || T.mixed[i] < 0 || T.mixed[i] > (int) Math.pow(2, varsNum) - 1){
								check = false;
								break;
							}
						}
					}
					
					if(T.mixed[T.mixed.length - 1] < 0 || T.mixed[T.mixed.length - 1] > (int) Math.pow(2, varsNum) - 1){
						check = false;
					}
					
					if(!check){
						JOptionPane.showMessageDialog(null, "You entered repeated numbers or numbers not between 0 and 2^n - 1 inclusive\n"
								+ "where n is the number of variables.");
					}
					
					else{
						
						
						if(T.mixed.length == (int) Math.pow(2, varsNum)){
							minPossibilitiesArea.append("F = 1");
						}
						
						
						else{
							// Processing.

							List binRepresentations = T.process();
							List primeImplicantsWithRepetitions = new List();
							List minTermsInvolved = new List();

							for (int i = 0; i < binRepresentations.size(); i++) {
								List a = ((List) binRepresentations.get(i));
								for (int j = 0; j < a.size(); j++) {
									if (((TabularMethod.matchedPairs) a.get(j)).check == false) {
										String str = ((TabularMethod.matchedPairs) a.get(j)).pattern;
										primeImplicantsWithRepetitions.add(str);
										List b = new List();
										b = ((TabularMethod.matchedPairs) a.get(j)).pairs;
										minTermsInvolved.add(b);
									}
								}
							}

							List.Node N = primeImplicantsWithRepetitions.getHead();
							String[] toArray = new String[primeImplicantsWithRepetitions.size()];

							for (int i = 0; i < toArray.length; i++) {
								toArray[i] = (String) N.getElement();
								N = N.getNext();
							}

							// Processing.

							// ==================================================================

							// Remove Duplicated Prime Implicants.
							List.Node K = primeImplicantsWithRepetitions.getHead();
							List filterTerms = new List();
							boolean flag;
							int finalSize = 0;
							for (int i = 0; i < toArray.length; i++) {
								String temp = (String) K.getElement();
								flag = false;
								for (int j = 0; j < i; j++) {
									if (temp.equals(toArray[j])) {
										filterTerms.add(i);
										flag = true;
										break;
									}
								}
								if (flag == false) {
									toArray[finalSize] = temp;
									finalSize++;
								}
								K = K.getNext();
							}

							String[] primeImplicants = new String[finalSize];

							for (int i = 0; i < finalSize; i++) {
								primeImplicants[i] = toArray[i];
							}

							for (int i = filterTerms.size() - 1; i >= 0; i--) {
								minTermsInvolved.remove((int) filterTerms.get(i));
							}
							// Remove Duplicated Prime Implicants.

							// ==================================================================

							// convert the prime implicants from binary to letters.

							for (int i = 0; i < primeImplicants.length; i++) {
								primeImplicants[i] = T.toLetters(primeImplicants[i]);
							}

							// convert the prime implicants from binary to letters.

							// ==================================================================

							// Creating the final table.

							int[][] table = new int[primeImplicants.length + 1][minTerms.length + 1];
							for (int i = 1; i < primeImplicants.length + 1; i++) {
								table[i][0] = i - 1;
							}
							for (int i = 1; i < minTerms.length + 1; i++) {
								table[0][i] = minTerms[i - 1];
							}

							T.primeImplicants = new String[primeImplicants.length];
							for (int i = 0; i < primeImplicants.length; i++) {
								T.primeImplicants[i] = primeImplicants[i];
							}

							// Creating the final table

							// ==================================================================

							// Displaying Prime Implicants.
							
							primeImplicantsArea.append("Consider the Function as: F(");
							String Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
							for (int i = 0; i < T.numOfVariables; i++) {
								primeImplicantsArea.append(Character.toString(Letters.charAt(i)));
								if (i < T.numOfVariables - 1) {
									primeImplicantsArea.append(", ");
								}
							}
							primeImplicantsArea.append(")\n");
							primeImplicantsArea.append("Then,\n");
							primeImplicantsArea.append("\nPrime Implicants:\n\n");
							for (int i = 0; i < primeImplicants.length; i++) {
								primeImplicantsArea.append((i + 1) + ") ");
								primeImplicantsArea.append(primeImplicants[i]);
								primeImplicantsArea.append(" Matched minTerms: ");
								for (int j = 0; j < ((List) (minTermsInvolved.get(i))).size(); j++) {
									int x = (int) ((List) (minTermsInvolved.get(i))).get(j);
									primeImplicantsArea.append("m" + x + " ");
									for (int k = 1; k < minTerms.length + 1; k++) {
										if (x == table[0][k]) {
											table[i + 1][k] = 1;
										}
									}
									if (j < ((List) (minTermsInvolved.get(i))).size() - 1) {
										primeImplicantsArea.append("- ");
									}
								}
								primeImplicantsArea.append("\n");
							}
							primeImplicantsArea.append("\n\n");
							
							// Displaying Prime Implicants.

							// ==================================================================

							T.table = new int[table.length][table[0].length];
							for (int i = 0; i < table.length; i++) {
								for (int j = 0; j < table[0].length; j++) {
									T.table[i] = table[i];
								}
							}

							// test minimum
							T.allPossible();

							
							
							
							minPossibilitiesArea.append("Minimum functions:\n\n");

							for (int i = 0; i < T.minimum.size(); i++) {
								minPossibilitiesArea.append("F" + (i + 1) + " = ");
								for (int j = 0; j < ((List) T.minimum.get(i)).size(); j++) {
									minPossibilitiesArea.append(primeImplicants[(int) (((List) T.minimum.get(i)).get(j))]);
									if (j < ((List) T.minimum.get(i)).size() - 1) {
										minPossibilitiesArea.append(" + ");
									}
								}
								minPossibilitiesArea.append("\n");
							}
							minPossibilitiesArea.append("\n\n");
						}
					}
						
				}
			}
		});

		JLabel TipLabel = new JLabel("tip : Enter Min terms and Don't Cares numbers separated by space");
		TipLabel.setBounds(293, 11, 481, 14);

		JLabel lblNewLabel = new JLabel("for example : Min Terms 1 2 4 5 9        Don't Cares 3 11 14 15");
		lblNewLabel.setBounds(303, 36, 463, 14);

		NumOfVariablesText = new JTextField();
		NumOfVariablesText.setBounds(158, 22, 62, 28);
		NumOfVariablesText.setHorizontalAlignment(SwingConstants.CENTER);
		NumOfVariablesText.setColumns(10);

		JLabel NumberOfVariablesLabel = new JLabel("Number Of Variables");
		NumberOfVariablesLabel.setBounds(9, 11, 140, 54);
		NumberOfVariablesLabel.setHorizontalAlignment(SwingConstants.CENTER);
		NumberOfVariablesLabel.setFont(new Font("Calibri", Font.PLAIN, 16));
		
		minPossibilitiesArea = new JTextArea();
		minPossibilitiesArea.setForeground(Color.BLACK);
		minPossibilitiesArea.setBackground(Color.WHITE);
		minPossibilitiesArea.setEditable(false);
		minPossibilitiesArea.setBounds(796, 155, 561, 543);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(MinTermsText);
		frame.getContentPane().add(MinTermsLabel);
		frame.getContentPane().add(DontCaresLabel);
		frame.getContentPane().add(DontCaresText);
		frame.getContentPane().add(SolveButton);
		frame.getContentPane().add(TipLabel);
		frame.getContentPane().add(lblNewLabel);
		frame.getContentPane().add(NumOfVariablesText);
		frame.getContentPane().add(NumberOfVariablesLabel);
		
		frame.setTitle("Quine McCluskey Method");
		
		frame.getContentPane().add(minPossibilitiesArea);
		primeImplicantsArea = new JTextArea();
		primeImplicantsArea.setForeground(Color.BLACK);
		primeImplicantsArea.setBackground(Color.WHITE);
		frame.getContentPane().add(primeImplicantsArea);
		primeImplicantsArea.setBounds(9, 155, 780, 543);
		primeImplicantsArea.setEditable(false);
	}
}
