/**
 * @author Ali Rihan && Mohamed Eid.
 */
package ITabular;

import Tabular.List;

public interface ITabularMethod {

	/**
	 * 
	 * @param num
	 *            a number to convert to binary
	 * @param numOfVariables
	 *            the number of variables in the function
	 * @return a string denotes the binary representation
	 */
	public String toBinary(int num, int numOfVariables);

	/**
	 * 
	 * @param binary
	 *            a String of a binary representation.
	 * @return the number of 1's in the String.
	 */
	public int numOfOnes(String binary);

	/**
	 * 
	 * @return a List contains the terms and their binary representations.
	 */
	public List createTable();

	/**
	 * 
	 * @param str1
	 *            first string that will be compared.
	 * @param str2
	 *            second string that will be compared.
	 * @return a string with the new pattern resulted from the comparing
	 *         process.
	 * @return null if the two strings differed in more than a single character.
	 */
	public String compare(String str1, String str2);

	/**
	 * 
	 * @return a List of all the tables containing the matched pairs and their
	 *         patterns.
	 */
	public List process();

	/**
	 * 
	 * @param binary
	 *            a binary representation which can be a pattern.
	 * @return a representation of the pattern with variables names A, B, ... .
	 */
	public String toLetters(String binary);

	/**
	 * 
	 * @param array
	 *            the covering table in last step.
	 * @return true if the table is all covered.
	 */
	public boolean isCovered(int[][] array);

	/**
	 * 
	 * @param array
	 *            the covering table in last step.
	 * @param row
	 *            specific index of a row that will be covered.
	 */
	public void cover(int[][] array, int row);

	/**
	 * copies two 2d arrays.
	 */
	public void copyTable();

	/**
	 * 
	 * @param arr
	 *            array of indices that will be combined.
	 * @param len
	 *            how many numbers will be chosen from the indices.
	 * @param startPosition
	 *            the position we start from to combine.
	 * @param result
	 *            temporary array used to store the combinations.
	 */
	public void combinations(int[] arr, int len, int startPosition, int[] result);

	/**
	 * generates all minimum possible combinations that creates the function
	 * from the prime implicants.
	 */
	public void allPossible();
}
